<?php

use Illuminate\Database\Seeder;

class QuestionAnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'question' => 'Hi',
                'answer' => 'Hello, Welcome to e-store. You can ask questions here. :)',
            ],
            [
                'question' => 'hello',
                'answer' => 'Hello, Welcome to e-store. You can ask questions here. :)',
            ],
            [
                'question' => 'what kind of product do you have?',
                'answer' => 'We have many kinds of products like, Shirts, Pents, Baby Suits, trousers ... etc. Which one do you want? ',
            ],
            [
                'question' => 'I want Shirt',
                'answer' => 'which color? we have white, blue and black. For more info visit our site.',
            ],
            [
                'question' => 'I am interested in Baby Suits',
                'answer' => 'We have variety of suits. From low to high price range. For more info visit our site.',
            ],
            [
                'question' => 'What are your delivery Charges?',
                'answer' => 'It depends on the weight of product. Usually we charge about RS 200/-',
            ],
            [
                'question' => 'I need contact information',
                'answer' => 'Please contact us on, Phone Number: +923317931991, Email: rs.mahaar@gmail',
            ],
            [
                'question' => 'Bye',
                'answer' => 'Bye. Thank you for your time. ',
            ]
        ];

        \App\QuestionAnswer::insert($data);
    }
}
