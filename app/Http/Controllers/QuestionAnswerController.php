<?php

namespace App\Http\Controllers;

use App\QuestionAnswer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class QuestionAnswerController extends Controller
{

    public function getAnswer(Request $request)
    {
        $sessionChatMsgs = session()->get('chatMsgs') ?: [];

        $msg = $request->msg;

        $data = [
            'actor' => 'user',
            'text' => $msg,
            'time' => Carbon::now()->toDateTimeString(),
        ];

        array_push($sessionChatMsgs, $data);

        $questionAnswer = QuestionAnswer::where('question','like', '%'.$msg.'%')->first();

        if (empty($questionAnswer))
        {
            $msg = "Sorry! I am unable to understand your question";
        }
        else
        {
            $msg = $questionAnswer->answer;
        }
        $data = [];
        $data = [
            'actor' => 'bot',
            'text' => $msg,
            'time' => Carbon::now()->toDateTimeString(),
        ];
        array_push($sessionChatMsgs, $data);
        session(['chatMsgs' => $sessionChatMsgs]);
        return response()->json($data);


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

}
