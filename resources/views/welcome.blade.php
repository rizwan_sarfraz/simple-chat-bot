<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        {{--<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>--}}
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet"

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow-y: hidden;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .container{max-width:1170px; margin:auto;}
            img{ max-width:100%;}
            .inbox_people {
                background: #f8f8f8 none repeat scroll 0 0;
                float: left;
                overflow: hidden;
                width: 40%; border-right:1px solid #c4c4c4;
            }
            .inbox_msg {
                border: 1px solid #c4c4c4;
                clear: both;
                overflow: hidden;
                width: 50%;
                float: right;
                /*margin-top: -43px;*/
            }
            .top_spac{ margin: 20px 0 0;}


            .recent_heading {float: left; width:40%;}
            .srch_bar {
                display: inline-block;
                text-align: right;
                width: 60%;
            }
            .headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

            .recent_heading h4 {
                color: #05728f;
                font-size: 21px;
                margin: auto;
            }
            .srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
            .srch_bar .input-group-addon button {
                background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
                border: medium none;
                padding: 0;
                color: #707070;
                font-size: 18px;
            }
            .srch_bar .input-group-addon { margin: 0 0 0 -27px;}

            .chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
            .chat_ib h5 span{ font-size:13px; float:right;}
            .chat_ib p{ font-size:14px; color:#989898; margin:auto}
            .chat_img {
                float: left;
                width: 11%;
            }
            .chat_ib {
                float: left;
                padding: 0 0 0 15px;
                width: 88%;
            }

            .chat_people{ overflow:hidden; clear:both;}
            .chat_list {
                border-bottom: 1px solid #c4c4c4;
                margin: 0;
                padding: 18px 16px 10px;
            }
            .inbox_chat { height: 550px; overflow-y: scroll;}

            .active_chat{ background:#ebebeb;}

            .incoming_msg_img {
                display: inline-block;
                width: 6%;
            }
            .received_msg {
                display: inline-block;
                padding: 0 0 0 10px;
                vertical-align: top;
                width: 92%;
            }
            .received_withd_msg p {
                background: #ebebeb none repeat scroll 0 0;
                border-radius: 3px;
                color: #646464;
                font-size: 14px;
                margin: 0;
                padding: 5px 10px 5px 12px;
                width: 100%;
            }
            .time_date {
                color: #747474;
                display: block;
                font-size: 12px;
                margin: 8px 0 0;
            }
            .received_withd_msg { width: 57%;}
            .mesgs {
                float: left;
                padding: 30px 15px 0 25px;
                width: 100%;
            }

            .sent_msg p {
                background: #05728f none repeat scroll 0 0;
                border-radius: 3px;
                font-size: 14px;
                margin: 0; color:#fff;
                padding: 5px 10px 5px 12px;
                width:100%;
            }
            .outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
            .sent_msg {
                float: right;
                width: 46%;
            }
            .input_msg_write input {
                background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
                border: medium none;
                color: #4c4c4c;
                font-size: 15px;
                min-height: 48px;
                width: 100%;
            }

            .type_msg {border-top: 1px solid #c4c4c4;position: relative;}
            .msg_send_btn {
                background: #05728f none repeat scroll 0 0;
                border: medium none;
                border-radius: 50%;
                color: #fff;
                cursor: pointer;
                font-size: 17px;
                height: 33px;
                position: absolute;
                right: 0;
                top: 11px;
                width: 33px;
            }
            .messaging { padding: 0 0 50px 0;}
            .msg_history {
                height: 516px;
                overflow-y: auto;
            }
            textarea:focus, input:focus{
                outline: none;
            }
            @media screen and (max-width: 700px) {
                .inbox_msg{
                    width: 100% !important;
                }
                html, body {
                    overflow-y: auto;
                }
            }
        </style>
    </head>
    <body>
    <div class="container">

        <div class="messaging row">
            <div class="col-md-4">
                <h3>Simple Chat Bot</h3>
                <p>This is simple chat bot where you can ask questions like</p>
                <ul>
                    <li>Hi</li>
                    <li>Hello</li>
                    <li>what kind of product do you have?</li>
                    <li>I want Shirt</li>
                    <li>I am interested in Baby Suits</li>
                    <li>What are your delivery Charges?</li>
                    <li>I need contact information</li>
                    <li>Bye</li>
                </ul>
            </div>
            <div class="col-md-8 col-12">
                <div class="inbox_msg" >

                    <div class="mesgs">
                        <div class="msg_history" id="msgs-div">
                            @php
                                $msgs = session()->get('chatMsgs') ?: [];
                            @endphp

                            @foreach($msgs as $msg)

                                @if($msg['actor'] == 'bot')
                            <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="https://icon-library.net/images/bot-icon/bot-icon-28.jpg" alt="sunil"> </div>
                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                        <p>{{$msg['text']}}</p>
                                        <span class="time_date"> {{$msg['time']}}</span></div>
                                </div>
                            </div>
                                @else
                            <div class="outgoing_msg">
                                <div class="sent_msg">
                                    <p>{{$msg['text']}}</p>
                                    <span class="time_date"> {{$msg['time']}}</span></div>
                            </div>

                                @endif
                            @endforeach

                        </div>
                        <div class="type_msg">
                            <div class="input_msg_write">
                                <input type="text" class="write_msg" id="msg" placeholder="Type a message"  />
                                <button class="msg_send_btn" onclick="sendMsg()" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



            <script>
                $(document).ready(function () {
                    scrollToBottom();

                    $("#msg").keyup(function(e){
                        if (e.which == 13) {
                            sendMsg();
                        }

                    });
                });

                function scrollToBottom(){
                    var element = document.getElementById("msgs-div");
                    element.scrollTop = element.scrollHeight;
                }

                function sendMsg() {


                    var msg = $('#msg').val();

                    if (! msg){
                        return;
                    }
                    pushSentMsg({
                        text: msg,
                        time: '{{\Carbon\Carbon::now()->toDateTimeString()}}'
                    });
                    $('#msg').val('');

                    $.ajax({
                        url: "{{route('get-answer')}}",
                        type: "post",

                        data: {
                            _token: '{{csrf_token()}}',
                            msg : msg
                        },

                        beforeSend: function(){
                            // $('#user-info-form').loading('toggle');
                        },
                        complete: function(){
                            // $('#user-info-form').loading('toggle');
                        },
                        success: function(response) {
                            console.log(response);
                            pushReceiveMsg(response);
                            // alert('send');


                        },
                        error: function(response) {

                        }
                    });
                }

                function pushReceiveMsg(msg) {

                    $("#msgs-div").append("<div class=\"incoming_msg\">\n" +
                        "                        <div class=\"incoming_msg_img\"> <img src=\"https://icon-library.net/images/bot-icon/bot-icon-28.jpg\" alt=\"sunil\"> </div>\n" +
                        "                        <div class=\"received_msg\">\n" +
                        "                        <div class=\"received_withd_msg\">\n" +
                        "                        <p>"+msg.text+"</p>\n" +
                        "                    <span class=\"time_date\"> "+msg.time+"</span></div>\n" +
                        "                    </div>\n" +
                        "                    </div>");

                    scrollToBottom();


                }

                function pushSentMsg(msg) {
                    $("#msgs-div").append("<div class=\"outgoing_msg\">\n" +
                        "                                <div class=\"sent_msg\">\n" +
                        "                                    <p>"+msg.text+"</p>\n" +
                        "                                    <span class=\"time_date\"> "+msg.time+"</span> </div>\n" +
                        "                            </div>");

                    scrollToBottom();
                }
            </script>
    </body>
</html>
